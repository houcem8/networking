import Foundation

open class RequestBuilder<T> {

  public let baseUrlString: String

  public init(baseUrlString: String) {
    self.baseUrlString = baseUrlString
  }

  open func buildRequest(_ type: T) -> HTTPRequest {
    fatalError("Implement in subclasses")
  }
}
