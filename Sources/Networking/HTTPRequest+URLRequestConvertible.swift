import Foundation
import Alamofire

extension HTTPRequest: URLRequestConvertible {

  public func asURLRequest() throws -> URLRequest {
    let url = try baseUrlString.asURL()

    var urlRequest = try URLRequest(url: url.appendingPathComponent(path), method: Alamofire.HTTPMethod(method))
    urlRequest.allHTTPHeaderFields = headers
    urlRequest = try method.encoding.encode(urlRequest, with: parameters)

    return urlRequest
  }
}
