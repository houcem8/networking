import Foundation
import Alamofire

extension Alamofire.HTTPMethod {

  init(_ method: HTTPMethod) {
    switch method {
    case .get:
      self = .get

    case .post:
      self = .post

    case .put:
      self = .put

    case .delete:
      self = .delete
    }
  }
}
