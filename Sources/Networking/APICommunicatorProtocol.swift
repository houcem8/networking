import Foundation

typealias RequestResult = Result<Any, ResponseError>
typealias RequestCompletion = (RequestResult) -> Void

public protocol APICommunicator: class {
  var authenticator: AuthenticatorProtocol! { get set }
  func request(_ request: HTTPRequest, completion: @escaping (Result<Any, ResponseError>) -> Void)
  func request<T>(_ request: HTTPRequest, decoder: JSONDecoder?, completion: @escaping (Result<T, ResponseError>) -> Void) where T: Codable

  func authenticatedRequest(_ request: HTTPRequest, completion: @escaping (Result<Any, ResponseError>) -> Void)
  func authenticatedRequest<T>(_ request: HTTPRequest,
                               decoder: JSONDecoder?,
                               completion: @escaping (Result<T, ResponseError>) -> Void) where T: Codable

  func removeAllCachedResponses()
}

extension APICommunicator {

  func request<T>(_ request: HTTPRequest,
                  decoder: JSONDecoder? = JSONDecoder.iso8601DateCompatible,
                  completion: @escaping (Result<T, ResponseError>) -> Void) where T: Codable {
    self.request(request, decoder: decoder, completion: completion)
  }

  func authenticatedRequest<T>(_ request: HTTPRequest,
                               decoder: JSONDecoder? = JSONDecoder.iso8601DateCompatible,
                               completion: @escaping (Result<T, ResponseError>) -> Void) where T: Codable {
    self.authenticatedRequest(request, decoder: decoder, completion: completion)
  }
}

enum AuthenticatorError: Error {
  case userIsNotLoggedIn
  case failedToAuthenticate
  case unknown
}

public protocol AuthenticatorProtocol: AuthenticationStateProtocol {
  var authorizationHeader: AuthorizationHeader? { get }
  func logout()
}

public enum AuthorizationHeader {
  case sybil(token: String)

  var value: String {
    switch self {
    case .sybil(let token):
      return "Bearer CB-\(token)"
    }
  }
}

public protocol AuthenticationStateProtocol {
  var isUserLoggedIn: Bool { get }
}

extension JSONDecoder {

  static var iso8601DateCompatible: JSONDecoder {
    let decoder = JSONDecoder()
    decoder.dateDecodingStrategy = .iso8601
    return decoder
  }

  convenience init(dateFormat: String) {
    self.init()

    dateDecodingStrategy = .custom { decoder -> Date in
      let container = try decoder.singleValueContainer()
      let dateString = try container.decode(String.self)

      let dateFormater = DateFormatter()
      dateFormater.dateFormat = dateFormat
      if let date = dateFormater.date(from: dateString) {
        return date
      } else {
        throw DecodingError.dataCorruptedError(in: container,
                                               debugDescription: "Received wrong date: \(dateString)")
      }
    }
  }
}

// MARK: Decoding

extension JSONDecoder {

  func decode<Input, Output: Codable>(_ object: Input) -> Output? {
    guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
      return nil
    }

    return try? decode(Output.self, from: data)
  }
}
