import Foundation

public enum ResponseError: Error, Equatable, CustomStringConvertible {
  case invalidURL
  case timedOut
  case contentNotFound
  case cancelled
  case authenticationFailed
  case conflict
  case gone
  case badRequest
  case decodeError
  case unknown(code: Int, description: String)

  public var description: String {
    switch self {
    case .invalidURL:
      return "invalidURL"
    case .timedOut:
      return "timedOut"
    case .contentNotFound:
      return "contentNotFound"
    case .cancelled:
      return "cancelled"
    case .authenticationFailed:
      return "authenticationFailed"
    case .conflict:
      return "conflict"
    case .gone:
      return "gone"
    case .badRequest:
      return "badRequest"
    case .decodeError:
      return "decodeError"
    case .unknown(let code, let errorDescription):
      return "unknown: code=\(code), description=\(errorDescription)"
    }
  }
}

extension ResponseError {

  init(_ error: Error, statusCode: Int?) {
    if let statusCode = statusCode {
      switch statusCode {
      case 400:
        self = .badRequest

      case 404:
        self = .contentNotFound

      case 401:
        self = .authenticationFailed

      case 409:
        self = .conflict

      case 410:
        self = .gone

      case 13:
        self = .timedOut

      default:
        self = .unknown(code: statusCode, description: error.localizedDescription)
      }
    } else {
      let error = error as NSError

      switch error.code {
      case NSURLErrorTimedOut:
        self = .timedOut

      case NSURLErrorCancelled:
        self = .cancelled

      default:
        self = .unknown(code: error.code, description: error.debugDescription)
      }
    }
  }

  init(_ error: Error, response: HTTPURLResponse?) {
    self.init(error, statusCode: response?.statusCode)
  }
}
