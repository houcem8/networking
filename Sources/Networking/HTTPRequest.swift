import Foundation
import Alamofire
//
//typealias Parameters = [String: Any]
//typealias Headers = [String: String]

public struct HTTPRequest: CustomStringConvertible {
  let path: String
  let method: HTTPMethod
  let baseUrlString: String
  let parameters: [String: Any]?
  var headers: [String: String]
  let loggingName: String

  public init(path: String,
       method: HTTPMethod,
       baseUrlString: String,
       parameters: [String: Any]? = nil,
       headers: [String: String] = [:],
       loggingName: String) {
    self.path = path
    self.method = method
    self.baseUrlString = baseUrlString
    self.parameters = parameters
    self.headers = headers
    self.loggingName = loggingName
  }

  public var description: String {
    "Path: \(path), Method: \(method.rawValue), BaseUrl: \(baseUrlString), Parameters: \(parameters ?? [:]), Language: \(headers["Accept-Language"] ?? "")"
  }
}
