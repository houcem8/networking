import Foundation
import Alamofire

extension HTTPMethod {

  var encoding: ParameterEncoding {
    switch self {
    case .delete:
      return URLEncoding.default

    case .get:
      return URLEncoding.default

    case .post, .put:
      return JSONEncoding.default
    }
  }
}
